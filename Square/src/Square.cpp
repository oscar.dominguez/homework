#include <iostream>
/*
*esta funcion dibujar cuadrado de asteriscos con la long de lado especificado
*/
void DrawSquare(const unsigned int& lado)
{
	for (int i = 1; i <= lado; i++)
	{
		for (int j = 1; j <= lado-i; j++) //makes the spaces
		{
			std::cout << " ";
		}
		for (int o = 1; o <= i*2-1 ; o++) //this makes the tree, i makes the first half. i*2 makes the other half(tree without the tip ). 1*2-1 makes the other half without 1 (tree form, with tip). 
		{
			std::cout << "*";
		}
		std::cout << std::endl;
	}
}

void main(void)
{
	int value; 
	std::cin >> value; 
	if (value >= 1 && value <= 24) //limits 
	{
		DrawSquare(value);

		for (int a = 1; a <= 3; a++) //makes a line of "|" 
		{
			for (int b = 1; b <= ((value / 2) * 2); b++) //makes the spaces depending on the value 
			{
				std::cout << " ";
			}
			std::cout << "|" << std::endl;
		}
	}
	
}