#include <iostream>

/*
* //sae: .cpp
bloque de codigo se delimita con {}

cuando un bloque de codigo le ponemos nombre, se conoce en c++ como funcion
Las funciones llevan un valor de retorno y una lista de parametros, cualquiera de los dos puede ser void(nada)

a veces necesitamos cierto control sobre los bloques de codigo 
- agregar variables
- control de flujo (if/else , ciclos)

sintaxis 
if (condicion) // se evalua como verdadero o falso 
	bloque a ejecutar cuando es verdadero 
else 
bloquea a ejecutar cuando es falso

************************
switch (variable)
{
case valor1:
	codigo
	break; // generalmente siempre se usa... poner atencion al resto del curso :)

case valor2:
	codigo
	break; //
}
*********************************************************
* ciclos 
for (int i = 0, i<100, i++){ (inicializaciones; condiciones; implementos)
	codigo a repetir
}

while (condicion)
	codigo a repetir

do 
{
codigo a repetir
} while(condicion)

//////////////////
/////////////////
El unico caso donde el bloque de codigo no necesita llaves es cuando en los ciclos o en condicionales solo es una linea de codigo. 

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

OPERADORES
+  suma 
-  resta
*  multiplicacion 
/  Division
%  Modulo %%
=  asignacion
== comparacion 
!= diferente de
< menor que
> mayor que 
<= menor igual que
>= mayor o igual 
!  NOT
& and logico (bitwise) o referencia, dependiendo del contexto
&& and logico (opera a nivel comparacion). 
|  or logico (bitwise)
|| or logico 
^ xor
++ (prefijo, posfijo) incremento en 1 ejemplo ++i -> prefijo  i++ -> postfijo
-- (prefijo, posfijo) decremento en 1 ejemplo --i -> prefijo  i-- -> postfijo
+= suma y reasigna ejemplo x+= 10 es lo mismo a x = x+10
-= resta y reasigna 
*= multiplica y reasigna 
/= divide y reasigna 
&= AND bitwise y reasigna 
!= or bitwise y reasigna

, ... 

una "casting" es una conversion de tipos. 
(tipo deseado) tipo original //con parentesis
<< corrimiento de bits a la izq
>> corrimiento de bits a la derecha

*/


int preIncremento(int& x) //++x //esta es mas rapida
{
	x += 1;
	return x;
}

int postIncremento(int& x) //x++{
{
	int y = x;
	x += 1;
	return y;
}


int main(void)
{
	//tipo de dato + nombre de variable
	int iInt; //numeros enteros incluyendo negativos (si tiene "unsigned" no puede tener signo").
	float fFloat; //numeros decimales. siempre guarda puntos decimales aunque no se los pongas //double (variante de float)
	bool bBool; //veerdadero o falso (condiciones) 
	char c = 'b' - 'a';//puedes guardar un caracter como una letra. //0000 0001 //se utiliza el b# 7 para almacenar 0 - 127 // -126, 127

	std::cout << c << std::endl;
	std::cout << "HolaMundo" << std::endl;

	//ejemplo de for
	std::string text1 = "Text1";
	for (auto c : text1)
		std::cout << c;
	
	std::cout << std::endl;

////////////////////////////
	//Ejemplos de operadores 

	int a = 5, b = 2;
	std::cout << a + b << std::endl; //imprime 7.

	std::cout << a / b << std::endl; //imprime 2. // se trunca. 

	std::cout << (float)a / (float)b << std::endl; //imprime 2.5.

	std::cout << a / (float)b << std::endl; //imprime 2.5.

	std::cout << (float)a / b << std::endl; //imprime 2.5.

	std::cout << "Operadores logicos" << std::endl;

	std::cout << (true && false) << std::endl; //false 

	std::cout << (true || false) << std::endl; //true

	std::cout << (true ^ false) << std::endl; //true //verdadero cuando los datos son diferentes (XOR)
	/*
	true ^true = f
	true ^ false = V
	false ^ false = F
	else
	*/

	std::cout << "Particularidades" << std::endl;

	if (1 - 2) //-1
		std::cout << "TRUE" << std::endl;
	else
		std::cout << "False" << std::endl;

	if (0)
		std::cout << "TRUE" << std::endl;
	else
		std::cout << "False" << std::endl;

	///////////////
	//ejemplo de operadores ++ --
	std::cout << "--------------------------" << std::endl;
	std::cout << "Operadores" << std::endl;
	int x = 0, 
		y = 10;

	std::cout << x++ << std::endl; 
	std::cout << ++y << std::endl;
	std::cout << "---Answers---" << std::endl;
	std::cout << x << std::endl; 
	std::cout << y << std::endl;
	std::cout << "--------------------------" << std::endl;
	return 0;
} //clasee templada acepta cualquier tipo de obj