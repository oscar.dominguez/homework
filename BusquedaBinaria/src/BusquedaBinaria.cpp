#include <iostream> //cual es la dif entre paso pentre referencia y paso por valor  EXAMEN
#include <vector>//por ref le mandas una direccion y los cambios se hacen en ese lugar de la memoria y por valor, se hacen una copia de variable
//ventaja estoy modificando el valor completo por lo tanto no se duplica la memoria
//desventaja sin querer modifiquen las variables (para solucionar eso pon que el arreglo es const
class Search {
private:
	int binarySearch(std::vector<int> a, int n, int l, int r)
	{
		if (l > r) //los indices se cruzaron
			return -1; //regresamos indice no valido (no se encontro n)

		int midPosition = (l + r) / 2;
		if (a[midPosition] == n)
			return midPosition;
		else if (n > a[midPosition])
		{
			//esta del lado derecho
			l = midPosition + 1;
			return binarySearch(a, n, l, r);
		}
		else
		{
			//esta del lado izq
			r = midPosition - 1;
			return binarySearch(a, n, l, r);
		}
	} //son iguales o no las dos funciones pero con diferentes parametros? son diferentes funciones y no marcara error en el comp
public:
	int binarySearch(std::vector<int> a, int n)
	{
		return binarySearch(a, n, 0, a.size() - 1); //size regresara el tam del arreglo pero el ult elemento esta en una casilla antes
	}
};

void main()
{
	Search search;
	std::vector<int> a{ 2, 3, 5, 7, 10, 30, 31, 50 };
	int n = 7;
	std::cout << search.binarySearch(a, n);
}