#include <iostream>

typedef struct _node {
	int value;
	struct _node* next;
}Node;

typedef Node* ptr_Node;

ptr_Node Head;

void insertInto(ptr_Node& head, int value)
{
	//creation and ini of the node
	ptr_Node newNode = new Node();
	newNode->value = value;
	newNode->next = nullptr;
	if (head == nullptr)
	{
		head = newNode;
	}
	else
	{
		Node* temp = head;
		while (temp->next != nullptr)
		{
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

ptr_Node find(ptr_Node& head, const int& value)
{
	ptr_Node temp = head;

	while (temp != nullptr)
	{
		if (temp->value == value) //esto es igual a value == (*temp).value
		{
			return temp;
		}
		else
		{
			temp = temp->next;
		}
	}

	return nullptr;
}

void deleteNode(ptr_Node& head, const int& value)
{
	ptr_Node temp = head;

	if ((head != nullptr) && temp->value == value)
	{
		ptr_Node toSave = temp->next;
		delete head;
		head = toSave;
	}
	else
	{
		while (temp != nullptr)
		{
			if ((temp->next !=nullptr) && (temp->next)->value == value)
			{
				ptr_Node toDeleate = temp->next;
				temp->next = temp->next->next;
				delete toDeleate;
				return;
			}
			else
			{
				temp = temp->next;
			}
		}
	}
}

void main(void)
{
	Head = nullptr;
	insertInto(Head, 3);
	insertInto(Head, 20);
	insertInto(Head, 15);
	insertInto(Head, 100);

	std::cout << find(Head, 15) << std::endl;
	std::cout << find(Head, 200) << std::endl;

	deleteNode(Head, 3);
	std::cout << Head << std::endl;
}