#include <iostream>
#include <vector>

void printArray(int array[], int size)
{
	for (int i = 0; i < size ; i++) //ojo, no cometer el "of by one" (tipico errore de salirse del arreglo por una) //mas flexible
	{
		std::cout << array[i] << std:: endl;
	}
}

void printVector(const std::vector<int>& myVector) //second for //aqui eres mas especifico 
{
	for (unsigned int i = 0; i< myVector.size(); i++) //mas flexible
	{
		//aqui se tiene la posibilidad de controlar el indice (variable i) 
		//y por lo tanto las operaciones no necesariamente se aplican a todos
		std::cout << myVector[i] << std::endl;
	}
}

void printVector2(const std::vector<int>& myVector) //second for //uno y cada uno
{
	for (auto &dato: myVector) //operacion a todos los elementos de myVector
	{
		std::cout << dato << std::endl;
	}
}



void main(void)
{ //					0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	//sintaxis para arreglos en c++/c - tintaxys of the arrays in c/c++
	int myArray[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; //starts in 0; //tama�o estatico
	//std::cout << myArray[1] << std::endl; //2
	//std::cout << myArray[9];
	printArray(myArray, 10);
	std::cout << "--------" <<std::endl;

	int myArray2[5] = { 2, 4, 6, 8, 10 };
	printArray(myArray2, 5);
	std::cout << "--------" << std::endl;
	std::cout << "C++" << std::endl;

	//sintaxis de c++ 
	//							0.    1,   2,   3,   4,  5,  6, 7,   8,  9,  
	std::vector<int> myVector = { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 }; //tama�o dinamico
	printVector(myVector);
	std::cout << "-------- Second for" << std::endl;
	printVector2(myVector);
}