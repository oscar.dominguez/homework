#include <iostream> //bibliotecas del c++ *
#include "Vec2.h" //bibliotecas nuestras *directorios adicionales
#include "Vec4.h"
#include "Vec3.h"

void main(void)
{
	//crear una clase vec 2, 3, 4 para un vector de 4 dimensiones y definirle el operador +,- entre vec4
	// 	   //ejemplo vec4 v3 = v1 + v2;
	//y tambien definir operadores *, / con flotantes
	//		ejemplo:  v1 = v1 / 100.0; (x/100.0, y/100.0, z/100.0, w/100.0
	std::cout << "/////////////////v1///////////////\n";
	std::cout << std::endl;
	Vec2 v1(30,2);
	Vec2 v2(10,23);
	v1 = v1 + v2;
	v1.printValues();

	std::cout << "/////////////////v4///////////////\n";
	std::cout << std::endl;

	Vec4 v4(1,2,3,4);
	v4.printValues();

	std::cout << "/////////////////v4///////////////\n";
	std::cout << std::endl;

	Vec4 v4_1(10, 20, 30, 1);
	Vec4 v4_2(10,10,10,1);
	Vec4 v4_3 = v4_1 + v4_2;
	v4_3 = v4_3 * 100;
	v4_3.printValues();

	std::cout << "/////////////////v1///////////////\n";
	std::cout << std::endl;

	v1 = v1 + v2;
	v2 = v2 - v1;
	v1.printValues();

	std::cout << "/////////////////v3///////////////\n";
	std::cout << std::endl;

	Vec3 v3_1(10, 20, 30);
	Vec3 v3_2(10, 10, 10);
	Vec3 v3_3 = v3_1 * v3_2;
	v3_3 = v3_3 * 100;
	v3_3.printValues();
}