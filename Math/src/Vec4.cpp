#include "Vec4.h"

Vec4::Vec4()
{
	v = { 0.0f,0.0f,0.0f,0.0f };
}

Vec4::Vec4(const float& x, const float& y, const float& z, const float& w)
{
	v.push_back(x);
	v.push_back(y);
	v.push_back(z);
	v.push_back(w);
}

Vec4::Vec4(const Vec4& rhs) : Vec4()
{
	*this = rhs; 
}

bool Vec4::operator== (const Vec4& rhs)
{
	return (v[0] == rhs.v[0] && v[1] == rhs.v[1] && v[2] == rhs.v[2] && v[3] == rhs.v[3]);
}

Vec4& Vec4::operator= (const Vec4& rhs)
{
	if (&rhs != this)
	{
		v[0] = rhs.v[0];
		v[1] = rhs.v[1];
		v[2] = rhs.v[2];
		v[3] = rhs.v[3];
	}
	return *this;
}

Vec4& Vec4::operator+(const Vec4& rhs)
{
	v[0] = v[0] + rhs.v[0];
	v[1] = v[1] + rhs.v[1];
	v[2] = v[2] + rhs.v[2];
	v[3] = v[3] + rhs.v[3];
	return *this;
}

Vec4& Vec4::operator-(const Vec4& rhs)
{
	v[0] = v[0] - rhs.v[0];
	v[1] = v[1] - rhs.v[1];
	v[2] = v[2] - rhs.v[2];
	v[3] = v[3] - rhs.v[3];
	return *this;
}

Vec4& Vec4::operator* (const float& rhs)
{
	v[0] = v[0] * rhs;
	v[1] = v[1] * rhs;
	v[2] = v[2] * rhs;
	v[3] = v[3] * rhs;
	return *this;
}

Vec4& Vec4::operator/ (const float& rhs)
{
	v[0] = v[0] / rhs;
	v[1] = v[1] / rhs;
	v[2] = v[2] / rhs;
	v[3] = v[3] / rhs;

	return *this;
}

//Vec4& Vec4::operator* (const Vec4& rhs)
//{
	//no se puede hacer producto cruz si los vectores tienen 4 o mas valores,
	// ya que la formula solo acepta 3 valores por vector.
//}

void Vec4::printValues()
{
	std::cout << "x = " << v[0] << ", y = " << v[1] << ", z = " << v[2] << ", w = " << v[3] << std::endl;
}