#include "Vec3.h"
Vec3::Vec3() //constructor def
{
	v = { 0.0f, 0.0f,0.0f};
}

Vec3::Vec3(const float& x, const float& y, const float& z)
{
	v.push_back(x);
	v.push_back(y);
	v.push_back(z);
}

Vec3::Vec3(const Vec3& rhs) : Vec3()
{
	*this = rhs;
}

bool Vec3::operator== (const Vec3& rhs)
{
	return (v[0] == rhs.v[0] && v[1] == rhs.v[1] && v[2] == rhs.v[2]);
}

Vec3& Vec3::operator= (const Vec3& rhs)
{
	if (&rhs != this)
	{
		v[0] = rhs.v[0];
		v[1] = rhs.v[1];
		v[2] = rhs.v[2];
	}

	return *this;
}

//A  +  B
//lhs  rhs
Vec3& Vec3::operator+(const Vec3& rhs) //suma
{
	v[0] = v[0] + rhs.v[0];
	v[1] = v[1] + rhs.v[1];
	v[2] = v[2] + rhs.v[2];
	return *this;
}

Vec3& Vec3::operator-(const Vec3& rhs)
{
	v[0] = v[0] - rhs.v[0];
	v[1] = v[1] - rhs.v[1];
	v[2] = v[2] - rhs.v[2];
	return *this;
}

Vec3& Vec3::operator* (const float& rhs) //resta
{
	v[0] = v[0] * rhs;
	v[1] = v[1] * rhs;
	v[2] = v[2] * rhs;
	return *this;
}

Vec3& Vec3::operator/ (const float& rhs)
{
	v[0] = v[0] / rhs;
	v[1] = v[1] / rhs;
	v[2] = v[2] / rhs;

	return *this;
}

Vec3& Vec3::operator* (const Vec3& rhs)
{
	v[0] = (v[1] * rhs.v[2]) - (v[2] * rhs.v[1]);
	v[1] = -(v[0] * rhs.v[2]) - (v[2] * rhs.v[0]);
	v[2] = (v[0] * rhs.v[1]) - (v[1] * rhs.v[0]);
	return *this;
}

void Vec3::printValues()
{
	std::cout << "x = " << v[0] << ", y = " << v[1] << ", z = " << v[2] <<std::endl;
}