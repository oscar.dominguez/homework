#pragma once

#include "Vector.h"

class Vec4 : public Vector
{
public:
	Vec4();
	Vec4(const float& x, const float& y, const float& z, const float& w);
	Vec4(const Vec4& rhs);

	bool operator== (const Vec4& rhs);
	Vec4& operator= (const Vec4& rhs);
	Vec4& operator+(const Vec4& rhs);
	Vec4& operator-(const Vec4& rhs);
	Vec4& operator* (const float& rhs);
	Vec4& operator/ (const float& rhs);

	virtual void printValues();
};