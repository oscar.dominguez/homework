#pragma once
#include <vector>
#include <iostream>

class Vector
{
//private: solo esta clase tiene acceso a sus propiedades y metodos
//public: todos tienen acceso a las propiedades y metodos
protected: //hace las propiedades y metodos privados,
	//pero las clases que heredan las ven como publicas
	std::vector <float> v;

public: 
	//metodo virtual puro porque no va a tener implementacion
	//al haber al menos un metodo abstracto la clase se convierte 
	//en clase abstracta
	//no se pueden crear instancias de una clase virtual puro
	virtual void printValues() // = 0;
	{
	};
};