#pragma once

#include "Vector.h"

class Vec3 : public Vector
{
public:
	Vec3();
	Vec3(const float& x, const float& y, const float& z);
	Vec3(const Vec3& rhs);

	bool operator== (const Vec3& rhs);
	Vec3& operator= (const Vec3& rhs);
	Vec3& operator+(const Vec3& rhs);
	Vec3& operator-(const Vec3& rhs);
	Vec3& operator* (const float& rhs);
	Vec3& operator/ (const float& rhs);
	Vec3& operator* (const Vec3& rhs); //implementar el producto cruz

	virtual void printValues();
};
