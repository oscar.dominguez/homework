#include <iostream>

/*
*una func recursiva debe cumplir con
1 que se ejecute a si misma
2.que cada llamado se reduzca el problema
3. una(omas) condiciones de paro
*/
unsigned long factorial(const unsigned long& n)
{
	//caracteriztica 3 condicion de paro
	if (n <= 1)
	{
		return 1;
	}
	else
	{
		//caracteriztica 1 se ejecuta a si misma
		return n * factorial(n - 1); //caracteriztica 2 el problema se reduce en cada llamado 
	}
}

//eje en clase: serie de fibonacci
//0, 1, 1, 2, 3, 4, 5, 8, 13, 21, 34, 55, 89, 144
//fibon() => 0 cuando n = 0
//			 1 cuando n = 1
//			 fibo(n-1)+fibo(n-2) cuando n>1
unsigned long Fibonacci(const unsigned long& n)
{
	if (n <= 2)
	{
		return n;
	}
	else
	{
		return Fibonacci(n - 1) + Fibonacci(n-2);
	}
}

void main(void)
{
	//std::cout << factorial(5) << std::endl;
	std::cout << Fibonacci(0) << std::endl;
	std::cout << Fibonacci(1) << std::endl;
	std::cout << Fibonacci(2) << std::endl;
	std::cout << Fibonacci(3) << std::endl;
	std::cout << Fibonacci(12) << std::endl;
}