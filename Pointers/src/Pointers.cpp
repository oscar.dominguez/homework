#include <iostream>
#include <array>
//paso por valor
void myfunc_valor(int e)
{
	std::cout << "dentro de la funcion por valor, e: "<<e << std::endl;
	++e;
	std::cout << "Incrementado el valor de e: "<<e << std::endl;
}

//paso por referencia
void myfunc_referencia1(const int& e) //sintaxis en c++
{
	std::cout << "dentro de la funcion por referencia1, e: " << e << std::endl;
	//++e; omposible, the comp doesnt allow it 
	std::cout << "Incrementado el valor de e: " << e << std::endl;
}

//paso por referencia
void myfunc_referencia2(int* e) //sintaxis en c
{
	std::cout << "dentro de la funcion por referencia2, e: " << *e << std::endl;
	++(*e);
	std::cout << "Incrementado el valor de e: " << *e << std::endl;
}

void func_forStruct(const struct player& j)
{
	////
}

struct player {
	std::string nombre;
	int lifes;
	int points;
	int lastLevel;
};

typedef struct player usuario;

//otra manera de hacer un nuevo tipo de dato
typedef struct
{
	int power;
	int distance;
	int reloadTime;
}Weapon;

void func_forTypedef(const usuario& u)
{
	/////
}

void main(void)
{
	{
		int x = 10;
		int* pnt_x = &x;
		std::cout << x << std::endl; //prints the value of the x
		std::cout << pnt_x << std::endl; // prints the direction of the memory of pnt_X
		std::cout << *pnt_x << std::endl;//prints the value saved in the direction where pnt is pointing
		std::cout << &x << std::endl;// prints the direction where x is saved
		std::cout << "-------------------------------------- \n";

		//int* pi; //do not leave a pointer without a value

		int* pi = nullptr;
		std::cout << pi << "\n";

		pi = new int(30);
		//++pi; //this will make the direction move, thus will return a trash value.
		++(*pi);
		std::cout << *pi << std::endl; //prints the value 30+1;
		std::cout << pi << std::endl; //prints the direction of the memory where new is saved
		int x2 = *pi;
		std::cout << x2 << std::endl;
		std::cout << "-------------------------------------- \n";

		std::array<int, 10> arr{ 1,2,3,4,5,6,7,8,9,10 };
		int* parr = &arr[0];
		//std::cout << *parr << std::endl;

		for (int i = 0; i < 10; ++i)
		{
			std::cout << *parr << std::endl;
			++parr; //moving inside the array
		}
		std::cout << "-------------------------------------- \n";
	}
	//paso por valor y paso por referencia
	
	int v = 10;
	myfunc_valor(v);
	std::cout <<"dentro de la funcion main: "<< v << std::endl;

	myfunc_referencia1(v);
	std::cout << "dentro de la funcion main: " << v << std::endl;

	myfunc_referencia2(&v);
	std::cout << "dentro de la funcion main: " << v << std::endl;

	std::cout << "-------------------------------------- \n";


	struct player j1;
	j1.nombre = "ji";
	j1.points = 0;

	usuario j2;
	j2.lifes = 2;

	Weapon gun1;
	gun1.distance = 100;

	Weapon* gun2 = new Weapon();
	gun2->distance = 200;

	std::cout << gun1.distance << std::endl; //gun1 is not a pointer
	std::cout << gun2->distance << std::endl; //gun2 is a pointer


}