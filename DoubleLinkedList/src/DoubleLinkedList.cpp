#include <iostream>

struct _Node{
	int value; 
	_Node* prev;
	_Node* next;

	//constructor es una funcion que se ejecuta cuando se crea el objeto objeto y nunca mas se puede volver a ejecutar
	//se tiene que llamar igual que el objeto
	//con uno que definamos, c++ dejara de 
	//h y cpp por el orden(lo podras llamar en el orden que quiera), 
	//si no toco el archivo cpp se queda precompilado, solo compilar los que fueron compilados 
	//mas rapido
	_Node(const int& value)//parametros
	{
		this->value = value;
		this->next = nullptr;
		this->prev = nullptr;
	}
};

typedef struct _Node Node;

typedef Node* ptr_Node;

void insert(ptr_Node& head, const int& value)
{
	ptr_Node newNode = new Node(value);
	if (head == nullptr)
	{
		head = newNode;
	}
	else
	{
		Node* temp = head;
		while (temp->next != nullptr)
		{
			temp = temp->next;
		}
		temp->next = newNode;

		newNode->prev = temp;
	}
}

void print(ptr_Node& head)
{
	std::cout << head->value << std::endl;
	Node* temp = head->next;
	Node* temp2 = head;
	while (temp != nullptr)
	{
		std::cout << temp->value << std::endl;
		temp2 = temp;
		temp = temp->next;
	}
	std::cout << "---prev node---"<< std::endl;

	while (temp2 != nullptr)
	{
		std::cout << temp2->value << std::endl;
		temp2 = temp2->prev;
	}
}

void main(void)
{
	ptr_Node Head = nullptr;
	insert(Head, 10);
	insert(Head, 20);
	insert(Head, 30);
	insert(Head, 40);

	print(Head);
}