#pragma once
#include "Node.h"

class LinkedList {
private:
	Node *head,
		*tail;
	unsigned int size;

public:
	LinkedList();
	void insert(const int& value);
	void PrintList();
	void SortedInsert(const int& value);

	~LinkedList();
};