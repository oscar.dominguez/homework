#pragma once
#include <iostream>

class Node
{
public: //by default the class is private, if you want to use the properties, must be public 
	//properties // not varables because the are now part of a class
	int Value;
	Node* prev;
	Node* next;

	//method is the same as a function, but this one is inside a class
	Node(const int& value);
	void printValue();
};
