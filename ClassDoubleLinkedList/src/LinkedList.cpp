#include "LinkedList.h"

LinkedList::LinkedList() : head(nullptr), //this is a double Linked List
							tail(nullptr),
							size(0)
{
	std::cout << "Executing the constructor of linkedList" << std::endl;
}

void LinkedList::PrintList()
{
	Node* temp = head;
	while (temp != nullptr)
	{
		temp->printValue();
		std::cout << ", ";
		temp = temp->next;
	}
	std::cout << std::endl;
}

void LinkedList::insert(const int& value)
{
	Node* newNode = new Node(value);
	newNode->Value = value;
	newNode->next = nullptr;
	if (head == nullptr) //no list, first node
	{
		head = newNode;
		tail = newNode;
		size = 1;
	}
	else //therse one or more nodes
	{
		tail->next = newNode;
		newNode->prev = tail;
		tail = newNode;
		++size;
	}
}

void LinkedList::SortedInsert(const int& value)
{
	Node* newNode = new Node(value);
	Node* temp = head;
	if (head == nullptr)
	{
		head = tail = newNode;
	}
	else
	{
		if (newNode->Value <= head->Value)
		{
			head->prev = newNode; 
			newNode->next = head;
			head = newNode;
			size++;
		}
		else if (newNode->Value >= head->Value && newNode->Value < tail->Value)
		{
			temp = head->next->next;
			head->next->next = newNode;
			newNode->next = temp;
			temp = head;
			size++;
		}

		else if (newNode->Value > tail->Value)
		{
			tail->next = newNode;
			newNode->prev = tail;
			tail = newNode;
			size++;
		}
	}
}

LinkedList::~LinkedList()
{
	std::cout << "executing the destructor" << std::endl;
	tail = head;
	while (tail != nullptr)
	{
		head = head->next;
		delete tail;
		tail = head;
	}
	
}