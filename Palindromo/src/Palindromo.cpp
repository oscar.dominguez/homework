#include <iostream>
/*
Ejercicio: 
hacer una funcion que regrese true sila cadena es palindromo. 
en otro hacer regresar falso 

ejemplo: Anita lava la tina. se lee igual al derecho y al reves.
		2. a
		3. aa
		4. aba
		5. abba
		6. casasac
		7. ""
*/

bool isPalindrom(const std::string& text)
{
	std::string rev = "";
	for (int i = text.length() - 1; i >= 0; --i)
	{
		rev += text[i];
	}

	/*if (rev == text)
	{
		return true;
	}
	else
	{
		return false;
	}*/
	// oooooo 
	return rev == text;
}

//
// a b a 
bool isPalindrom2(const std::string& text)
{
	int last = text.length() - 1;
	for (int i = 0; i <= last; ++i)
	{
		if (text[i] != text[last - i])
		{
			return false;
		}
	}
	return true;
}

//version optimizada del ispalindom2 y que soporte los casos "aba" "abba" 
bool isPalindrom3(const std::string& text)
{
	int last = text.length() - 1;
	if (text.length() % 2 == 0)
	{
		for (int i = 0; i <= last; ++i)
		{
			if (text[i] != text[last - i])
			{
				return false;
			}
		}
		return true;
	}
	else if (text.length() % 2)
	{
		std::string rev = "";
		for (int i = text.length() - 1; i >= 0; --i)
		{
			rev += text[i];
		}
		return rev == text;
	}
}

//que soporte espacios ejemplo "Anita lava la tina"
bool isPalindrom4(const std::string& text)
{
	std::string rev = "";
	for (int a = 0; a < text.length(); ++a)
	{
		if (text[a] == ' ')
		{
			rev += text[a];
		}
	}

	int last = rev.length() - 1;

	for (int i = 0; i <= last; ++i)
	{
		if (rev[i] != rev[last - i])
		{
			return false;
		}
	}
	return true;
}

void main(void)
{
	std::string palabra = "abba"; // es lo mismo que std::vector<char> 
	//std::cout << palabra << std::endl;
	//se puede acceder a cada letra igual que como se hace con los vectores
	//ejemplo
	//std::cout << palabra[1] << std::endl; //imprime la letra que esta en el indice 1
	std::cout << isPalindrom3(palabra) << std::endl;
}