#include <iostream>
#include <string>
/*
base 2 = binario (0, 1) 0,1. 10, 11, 100       11b
base 8 = octal (0 - 7) 0, 1, 2, 3, 4, 5, 6, 7, 10, 11, 12... 17... 20. ... 77    77o
base 10 
base 16 = hexadecimal (0 - 9, A - F) 0... 9, A, B, C, D, E, F, 10, 11, 12, 13... 19, 1A, 1B... 1F, 20... 100 ejemplo: 0x1A

idea o algoritmo: dividir num entre la base y continuar dividiendo los cocientes hasta que el conciente de 0 y se concatenan los residuos en orden inverso y al final e primer cociente

ejemplo 1
num = 20 a base 16
(div) num/16= 1 y sobran 4       0x14
(mod) num % 16 = 4

ejemplo 2 a base 16 
num 300

300 / 16 = 18 
300 % 16 = 12
18 / 16 = 1
18 % 16 = 2
ultimo cociente + todos los residuos del ultimo hacia el primero "1" + "2" + "12" -> C"  = 12C

Ejemplo 3: 
num 20 a base 2 

1. 20/ 2 = 10 
2. 20 % 2 = 0
3. 10 / 2 = 5
4. 10 % 2 = 0
5. 5 / 2 = 2 
6. 5 % 2 = 1
7. 2 / 2 = 1
8. 2 % 2 = 0
res: 0001 0100

Ejemplo 4 
num 20 a base 8 
1. 20 / 8 = 2
2. 20 % 8 = 4
3. 2 / 8 = 0
4. 2 % 6 = 2

res: 024o
*/

//std::itoa(25);
//std::to_string (25) -> "25"

std::string BaseConverter(const unsigned int & num, const unsigned int & base)
{
	//tip: para representar un numero mayor que 9 como un solo digito, usaran letras y se pueden calcular mas o menos asi
	//if (value > 9 ) // value = 12
	//texto = texto + std::itoa('A' + (value - 10)
	int res = 0;
	int resi = 0;
	while (res <= 0)
	{
		res = num / base;
		resi = num % base;
	}
	return std::to_string(res) + std::to_string(resi);
}

int main(void)
{
	//mientras la base sea mayor que 1 y menor que 37, repetir
	int num, base;
	do
	{
		std::cin >> num;
		std::cin >> base;
		if (base > 2 && base < 37)
		{
			std::cout << BaseConverter(num, base) << std::endl;
			std::cout << "-------------" << std::endl;
		}
	} while (base > 1 && base < 37);
	return 0;
}