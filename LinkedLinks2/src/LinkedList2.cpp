#include <iostream>

typedef struct _node {
	int value;
	struct _node* next;
}Node;

typedef Node* ptr_Node;

ptr_Node Head;

void insertInto(ptr_Node& head, int value)
{
	//creation and ini of the node
	ptr_Node newNode = new Node();
	newNode->value = value;
	newNode->next = nullptr;
	if (head == nullptr)
	{
		head = newNode;
	}
	else
	{
		Node* temp = head;
		while (temp->next != nullptr)
		{
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void PrintLinkedList(ptr_Node& node)
{
	std::cout << node->value << std::endl;
	Node* temp = node->next;
	while (temp!= nullptr)
	{
		std::cout << temp->value << std::endl;
		temp = temp->next;
	}
}

void main(void)
{
	Head = nullptr;
	insertInto(Head, 15);
	insertInto(Head, 23);
	insertInto(Head, 38);
	insertInto(Head, 41);

	PrintLinkedList(Head);
}