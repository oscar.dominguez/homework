#pragma once
#include "Node.h"

class BinaryTree {
private:
	Node* root;
	void insertRecursive(ptrNode& root, ptrNode& newNode);
	void printRecursiveInFix(ptrNode& root);
	void printReversed(ptrNode& root);
	void printPreOrder(ptrNode& root);
	void printPostOrder(ptrNode& root);
	Node* find_recursive(ptrNode& root,const int& value);
public:
	void insert(const int& value);
	void printInfx(); //primer izq, luego referencia, luego el derecho
	//inOrder
	void printPreOrder(); //primero el de referencia, luego el izq, luego el derecho
	//PreOrder
	void printPostOrder(); //primero izq luego der, luego referencia
	//PostOrder
	void printReversed();

	Node* find(const int& value);

	BinaryTree();
};