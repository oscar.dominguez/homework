#include <iostream>
#include "BinaryTree.h"

void main()
{
	BinaryTree tree;
	tree.insert(5);
	tree.insert(3);
	tree.insert(6);
	tree.insert(4);
	tree.insert(1);
	tree.insert(8);
	tree.insert(2);
	tree.insert(7);
	tree.insert(9);
	tree.insert(10);

	std::cout << "///print in order///" << std::endl;
	tree.printInfx();
	std::cout << std::endl;
	std::cout << "///print in revrse///" << std::endl;
	tree.printReversed();
	std::cout << std::endl;
	std::cout << "///print pre order///" << std::endl;
	tree.printPreOrder();
	std::cout << std::endl;
	std::cout << "///print post order///" << std::endl;
	tree.printPostOrder();

	tree.find(12);
	tree.find(5);
	tree.find(2);
}