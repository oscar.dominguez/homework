#include <iostream>
#include "BinaryTree.h"
#include "Node.h"

void BinaryTree::insertRecursive(ptrNode& root, ptrNode& newNode)
{
	if (root == nullptr) //caso 1 no existe el nodo de referencia
	{
		root = newNode;
	}
	else if (newNode->value < root->value)//caso 2 se tiene que insertar por la izq
	{
		insertRecursive(root->left, newNode);
	}
	else //caso 3: se tiene que insertar por la derecha por ser mayor o igual
	{
		insertRecursive(root->right, newNode);
	}
}

void BinaryTree::printRecursiveInFix(ptrNode& root)
{
	if (root == nullptr)
		return;

	printRecursiveInFix(root->left);
	std::cout << root->value << std::endl;
	printRecursiveInFix(root->right);
}

void BinaryTree::printReversed(ptrNode& root)
{
	if (root == nullptr)
	{
		return;
	}
	if (root->right != nullptr)
	{
		printReversed(root->right);
	}

	std::cout << root->value << std::endl;

	if (root->left != nullptr)
	{
		printReversed(root->left);
	}
}

void BinaryTree::printPreOrder(ptrNode& root)
{
	if (root == nullptr)
		return; 
	std::cout << root->value << std::endl;
	printPreOrder(root->left);
	printPreOrder(root->right);
}

void BinaryTree::printPostOrder(ptrNode& root)
{
	if (root == nullptr)
		return;
	printPostOrder(root->left);
	printPostOrder(root->right);
	std::cout << root->value << std::endl;
}

void BinaryTree::insert(const int& value)
{
	Node* temp = new Node(value);
	insertRecursive(root, temp);
}

void BinaryTree::printInfx()
{
	printRecursiveInFix(root);
}

void BinaryTree::printPreOrder()
{
	printPreOrder(root);
}

void BinaryTree::printPostOrder()
{
	printPostOrder(root);
}

void BinaryTree::printReversed()
{
	printReversed(root);
}

Node* BinaryTree::find_recursive(ptrNode& root, const int& value)
{
	if (root->value == value)
		return root;
	else if ((value < root->value) && (root->left != nullptr))
	{
		return find_recursive(root->left, value);
	}
	else if ((value >= root->value) && (root->right != nullptr))
	{
		return find_recursive(root->right, value);
	}
	return nullptr;
}

Node* BinaryTree::find(const int& value)
{
	if (root != nullptr)
	{
		return find_recursive(root, value);
	}
	else
	{
		return nullptr;
	}
}

BinaryTree::BinaryTree() : root(nullptr)
{

}