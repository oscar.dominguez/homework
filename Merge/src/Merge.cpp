#include <iostream>
#include <vector>

//a y b tienen que estar ordenados
std::vector<int> merge(const std::vector<int>& v1, const std::vector<int>& v2)
{
	int i = 0,
		j = 0;
	std::vector<int> result;
	bool loop = true;
	do
	{
		if (v1[i] <= v2[j])
		{
			result.push_back(v1[i]);
			++i;
		}
		else
		{
			result.push_back(v2[j]);
			++j;
		}
		if (i == v1.size())
		{
			for (int a = 0; a < v1.size(); ++a)
			{
				result.push_back(v1[a]);
			}
			loop = false;
		}
		else if (j == v2.size())
		{
			for (int b = 0; b < v2.size(); ++b)
			{
				result.push_back(v2[b]);
			}
			loop = false;
		}
	} while (loop);
	return result;
}

/*
bubble sort

1. comparar un elemento contra todos los que estan adelante e intercambia las posiciones cuando el que esta adelante es mas peque
2. repetir el paso 1, con el elemento siguiente, hasta que lleguemos al penultimo elemento

ej.
p1
i  j  
3, 2, 1... si es mas peque�o, intercambiamos 

i     j
2, 3, 1... si es mas peque�o intercambiamos. en este punto ya se comparo el 2 contra todos los de adelante, pasamos al p2

p2
  i j
1,3,2 ... si es mas peque�o intercambiamos (queda 1, 2, 3).

Nota: como se intercambian los valores entre 2 variables? 
int a = 3, b = 5;
a = b; en este momento a y b valen
b = temp; //en este momento a = 5 y b = 3;

*/

std::vector<int> sort(const std::vector<int>& v)
{
	int i = 0,
		j = 0;
	std::vector<int> result(v.size());

	for (int a = 0; a < v.size(); ++a)
	{
		result[a] = v[a];
	}

	for (int i = 0; i < result.size(); ++i)
	{
		for (int j = i; j < result.size(); ++j)
		{
			if (result[i] > result[j])
			{
				int temp = result[i];
				result[i] = result[j];
				result[j] = temp;
			}
		}
	}

	return result;
}


//v1 y v2 no se espera que esten ordenados, pero el resultado si debe estar ordenado
std::vector<int> unsorted_merge(const std::vector<int>& v1, const std::vector<int>& v2)
{ // se esta implementando la opcion 2
	int i = 0,
		j = 0;
	bool loop = true;
	std::vector<int> v3;
	while (loop)
	{
		if (v1[i] <= v2[j])
		{
			v3.push_back(v1[i]);
			++i;
		}
		else
		{
			v3.push_back(v2[j]);
			++j;
		}
		if (i == v1.size())
		{
			for (int a = 0; a < v1.size(); ++a)
			{
				i = 0;
			}
			loop = false;
		}
		else if (j == v2.size())
		{
			for (int b = 0; b < v2.size(); ++b)
			{
				j = 0;
			}
			loop = false;
		}
	}

	return v3;
}

void printVector(const std::vector<int>& myVector3)
{
	for (auto& dato : myVector3)
	{
		std::cout << dato << ", ";
	}
}

/*
* op1
	1 hacer v3 = resultado de merge juntando v1 y v2
	2 ordenar v3 con un algoritmo de ordenamiento que hagamos 
	2.1

	op2
	1. crear un algoritmo de ordenamiento
	1. ordenar v1
	2. ordenar v2
	3 hacer v3 = resultado de merge juntando v1 y v2

	op3
	1. hacer v3 = v1+ v2
	2. crear algoritmo de ordenamiento
	3. ordenar v3

*/

void main(void)
{
	std::vector<int> a{ 7, 9, 1, 5, 3 },
		b{ 6, 8, 2, 7, 4 };

	std::vector<int> c = sort(a); //sort v1
	std::vector<int> d = sort(b); //sort v2
	std::vector<int> e = unsorted_merge(c, d); // new v3
	std::cout << "Vector 3, size: " << e.size() << std::endl;
	printVector(e);
}