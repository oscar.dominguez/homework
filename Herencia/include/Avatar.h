#pragma once
#include "Ship.h"
#include <iostream>
//la sig 2 clases deberian estar en un arch aparte pero,
//la pongo aqui para facilitar la explicacion
class NetworkAccess
{
public:
	int ID;
	int blueToothConnection_ID;
	int blueToothConnectionState; //connected, disconnected, connecting, disconnecting

	void connect()
	{
		blueToothConnectionState = 1;
	}

	void disconnect()
	{
		blueToothConnectionState = 0;
	}
};

class PowerUp
{
public:
	float energy;
	int gunType;
	float speed;
	int type;
public:
	PowerUp(const float& energy,
		const int& gunType,
		const float& speed,
		const int type) : type(type),
		energy(energy),
		gunType(gunType),
		speed(speed)
	{
	}
};

class Avatar : public Ship 
{
private:
	std::string name;
	NetworkAccess controller;
public:
	
	Avatar(const std::string& name);
	//destructor
	~Avatar();//jamas lleva parametros porque el constructor 
		      //se ejecuta unicamente cuando la variable esta por destruirse
	Avatar& operator+(const PowerUp& powerup);
	Avatar& operator+(const Ship& ship);
};
//que gano al separar los archivos
//el comp es inteligente como para identificar cuando un 