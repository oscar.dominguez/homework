#pragma once

//que es el metodo constructor
//es la primera funcion que se llama al instanciarse un obj
class Ship {
private:
	float posx, posy;
	int type;
	float energy;
	int gunType;
	float speed;

public:
	Ship();
	Ship(const int& type);
	void move(const float& x, const float& y);
	void shoot(const float& x, const float& y);
	void evolve();
	bool colider();

	void setEnergy(const float& value);
	void setType(const int& value);
	void setGunType(const int& value);
	void setSpeed(const float& value);//el const indica que no se podra modificar el valor de la variable value

	float getEnergy() const; //el const indica que no se puede modificar ninguna prop de ship dentro de ese metodo
	int getType() const;
	int getGunType()const;
	float getSpeed()const;
	//capsulacion: 

	~Ship();
};