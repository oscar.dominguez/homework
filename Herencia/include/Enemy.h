#pragma once
#include "Ship.h"

class Enemy : public Ship 
{
private:
	int hp;

public:
	Enemy();
	Enemy(const int& type);
	virtual void evolve();

	~Enemy();
};