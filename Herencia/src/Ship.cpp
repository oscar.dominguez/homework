#include <iostream>
#include "Ship.h"

Ship::Ship(): posx(0),
			  posy(0),
			  type(0),
			  energy(0),
			  gunType(0),
			  speed(0)
{
	std::cout << "Ship::constructor" << std::endl;
}

Ship::Ship(const int& type) : posx(0),
							  posy(0),
							  type(type),
							  energy(0),
							  gunType(0),
							  speed(0)
{
	std::cout << "Ship::constructor(type)" << std::endl;
}

void Ship::move(const float& x, const float& y)
{
	std::cout << "Ship::move" << std::endl;
}
void Ship::shoot(const float& x, const float& y)
{
	std::cout << "Ship::shoot" << std::endl;
}
void Ship::evolve()
{
	++type;
	speed += 2;
	std::cout << "Ship::evolve" << std::endl;
}
bool Ship::colider()
{
	std::cout << "Ship::colider" << std::endl;
	return true;
}

void Ship::setEnergy(const float& value)
{
	if (value >= 0)
		energy = value;
	else
		energy = 0;
}

void Ship::setType(const int& value)
{
	if (value >= 0)
		type = value;
	else
		type = 0;
}

void Ship::setGunType(const int& value)
{
	gunType = value;
}

void Ship::setSpeed(const float& value)
{
	speed = value;
}

float Ship::getEnergy() const
{
	return energy;
}

int Ship::getType() const
{
	return type;
}

int Ship::getGunType() const
{
	return gunType;
}

float Ship::getSpeed() const
{
	return speed;
}

Ship::~Ship()
{
	std::cout << "Ship~Ship" << std::endl;
}