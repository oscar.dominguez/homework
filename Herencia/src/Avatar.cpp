#include "Avatar.h"

Avatar::Avatar(const std::string& name) : Ship(),
										  name(name) //propiedas(parametro)
{
	std::cout << "Avatar::Constructor" << std::endl;
}

Avatar::~Avatar()
{
	if (controller.blueToothConnectionState == 1)
		controller.disconnect();
	std::cout << "Avatar~Avatar" << std::endl;
}

Avatar& Avatar::operator+(const PowerUp& powerup) //los operadpores siempre regresan una referencia
{
	setEnergy(getEnergy()+powerup.energy);
	setType(getType() + powerup.type);
	setGunType(getGunType() + powerup.gunType);
	setSpeed(getSpeed() + powerup.speed);
}

Avatar& Avatar::operator+(const Ship& ship)
{
	setEnergy(getEnergy() + ship.getEnergy());
	setType(getType() + ship.getType());
	setGunType(getGunType() + ship.getGunType());
	setSpeed(getSpeed() + ship.getSpeed());

}