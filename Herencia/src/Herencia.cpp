#include <iostream>
#include "Ship.h"
#include "Enemy.h"
#include "Avatar.h"

void addPowerUp(Avatar& avatar, const PowerUp& powerUp)
{
	float energy = avatar.getEnergy() + powerUp.energy;
	avatar.setEnergy(energy);

	avatar.setType(avatar.getType()+ powerUp.type);
}

void main()
{
	Ship ship1;
	Enemy enemy1;
	Enemy enemy2(1); //�cuando el c++ no crea el const default
	Avatar avatar1("Player1"); //en cuanto yo defino un const se dejan de construir los default
	PowerUp power1(50,0,10,0);

	ship1.evolve();
	enemy1.evolve();
	enemy2.evolve();
	avatar1.evolve();
	avatar1.setEnergy(50);
	//addPowerUp(avatar1,power1);
	avatar1 = avatar1 + power1; //equivale a escribir: avatar1 = avatar1.+(power1)
	avatar1 = avatar1 + ship1;//avatar hereda de ship por lo tanto se pueden sumar los avatares
	avatar1 = avatar1 + enemy1;//enemy tambien es ship, hereda de ship.
	//enemy1 = enemy1 + enemy2; //ship/enemy no tiene ningun operador redefinido para suma
	//no esta definido el operador + para la clase ship
	//avatar1 = ship + avatar1; //ERROR ship no tiene un metodo suma
}