#include "Enemy.h"
#include <iostream>

Enemy::Enemy() : hp(100)
{
	std::cout << "Enemy::constructor" << std::endl;
}
Enemy::Enemy(const int& type) : Ship(type),
								hp(100+10*type)
{
	std::cout << "Enemy::constructor(type)" << std::endl;
}

void Enemy::evolve()
{
	Ship::evolve();
	hp += 10;
	std::cout << "Enemy::evolve" << std::endl;
}

Enemy::~Enemy()
{
	std::cout << "Enemy~Enemy" << std::endl;
}