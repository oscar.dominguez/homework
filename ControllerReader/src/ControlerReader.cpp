#include <iostream>
/*
Completar de tarea
Imprimir todos los estados del control dado un valor
*/

/*
* los cuatro bits menos significativos (lsb) son de la palanca
* los cuatro bits mas significativos (Msb) son de los botones 
bit 0 = arriba 
bit 1 = derecha
bit 2 = abajo
bit 3 = izquierda


bit 4 = Triangulo
bit 5 = circulo
bit 6 = equis
bit 7 = cuadrado

ejemplo 
/0001 1000 = 24 (izquierda y triangulo).

ejemplo 0110 1100 = izquierda abajo, circulo y equis. = 102

OJO: usar mascaras de bits para saber que esta prendido y apagado 
una mascara es un valor que lleva los bits que queremos prendidos o apagados y que se aplicara con una peracion binaria al valor original. 

ejemplo:
mascara 0000 0001 = 1
value	0110 0001 = 97
	value & mascaa = 1 //eso quiere decir que el bit 0 esta encendido 
Resultado 0000 0001 = 1. 
*/


void printControllerState(const unsigned char & value)
{
	//imprimir los estados de los botones y la palanca
	if (value & 1)
	{
		std::cout << "Joystick Up. ";
	}

	if (value & 2)
	{
		std::cout << "Joystick Right. ";
	}

	if (value & 4)
	{
		std::cout << "Joystick Down.  ";
	}
	
	if (value & 8)
	{
		std::cout << "Joystick Left.  ";
	}

	if (value & 16)
	{
		std::cout << "Triangle.  ";
	}

	if (value & 32)
	{
		std::cout << "Circle.  ";
	}

	if (value & 64)
	{
		std::cout << "X.  ";
	}

	if (value & 128)
	{
		std::cout << "Square.  ";
	}
}

void main(void)
{
	for (int i = 0; i < 20; i++)
	{
		unsigned char value = rand() % 225;
		std::cout << i << " ->";
		printControllerState(rand() % 225);
		std::cout << std::endl;
	}
}